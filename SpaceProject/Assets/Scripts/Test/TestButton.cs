﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Test
{
    public class TestButton : MonoBehaviour
    {
        public void Next()
        {
            SceneManager.LoadScene("Game3");
        }

        public void Exit()
        {
            Application.Quit();
        }
    }
}
